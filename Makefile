dev:
	python app.py
run:
	python app.py
setup:
	sudo apt install git make python-is-python3 python3-pip graphviz git-lfs
	pip3 install -r requirements.txt
