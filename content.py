from index import app
from pages.callbacks import callbacks1
from pages.callbacks import callbacks_overview
from pages.callbacks import callbacks_tasks
from pages.callbacks import callbacks_users
from pages.callbacks import callbacks_graph
from pages.overview import overview
from dash import html, dcc
import dash_dangerously_set_inner_html

tabs_styles = {
    'height': '5vh'
}

tab_style = {
    'borderBottom': '1px solid #d6d6d6',
    'padding': '8px',
    'fontWeight': 'bold'
}

# define layout
app.layout = html.Div([
    dcc.Store(id='df_tasks_storage', storage_type='session'),
    dcc.Store(id='tag_index_storage', storage_type='session'),
    dcc.Store(id='df_users_storage', storage_type='session'),
    dash_dangerously_set_inner_html.DangerouslySetInnerHTML("<style>body { overflow-x: hidden; background-color: rgb(168, 211, 237); }</style>"),
    dcc.Tabs(id='tabs-nav', 
        value='overview', 
        children=[
            dcc.Tab(label='Overview', value='overview', style=tab_style, selected_style=tab_style),
            dcc.Tab(label='Explore tasks', value='tasks', style=tab_style, selected_style=tab_style),
            dcc.Tab(label='User progress: Submissions', value='users', style=tab_style, selected_style=tab_style),
            dcc.Tab(label='User progress: Dependency graph', value='graph', style=tab_style, selected_style=tab_style),
        ],
        style=tabs_styles
    ),
    html.Div(id='tabs-nav-content'),
    ], style={'width':'100vw', 'height':'93vh'})

