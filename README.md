As of writing this there is a demo version live: https://c109-227.cloud.gwdg.de/
This repository has lfs disabled in order to limit access to the database.

## About this dashboard and its data
The Informatik Propädeutikum took place for the first time in 2021. It's target audience are bachelor freshmen from Göttingens computer science programs. The second weeks topic is python programming. Students learn the basics in a self-directed way with an online system, the [SmartBeans](https://gitlab.gwdg.de/smart/smartbeans), originally developed at Uni Göttingen for a C programming course. 

This dashboard makes the course data from 2021 available and explorable. The content is grouped into 4 tabs where you can explore different aspects of the data like tasks, submitted solutions, learning behaviour and the prerequisites structure.
You can usually interact with plots and hover, zoom or select data. All student data is handled pseudonymously.

## Setup

In principle, this is an lfs-enabled repository. It requires `git-lfs` to be installed. To fetch the database you also need to pull it with lfs:
```
git clone https://gitlab.gwdg.de/smartbeans-visualization-project/database.git
cd database
git lfs pull
```

To install all dependencies (system packages and pip modules) just run `make setup`.

## Run the project
```
make run
```