import sqlite3
import graphviz
import json

from hsluv import hsluv_to_hex
import colorcet as cc
#cmap = cc.b_linear_wyor_100_45_c55
#cmap = cc.b_isoluminant_cm_70_c39
cmap = cc.b_linear_wcmr_100_45_c42
from math import floor

def getData(db_file = "./db.sqlite", sqlquery = None):
    # open database
    conn = sqlite3.connect(db_file)
    cursor = conn.execute(sqlquery)
    return cursor.fetchall()

def constructGraph(filepath = "./pages/svg/"):
    dot = graphviz.Digraph(node_attr={"style": "filled", "fontsize": "150"}, graph_attr={"ratio": "1", "size": "19!"}, edge_attr={"arrowhead": "normal"})

    data = getData(sqlquery = """
        SELECT taskid, prerequisites, taskDescription
         FROM (SELECT * from tasks LEFT JOIN courseTask on tasks.taskid = courseTask.taskid) WHERE course = "propäd";
        """)

    m, solved = freqSolved()
    node_ids = []

    for row in data:
        taskid = row[0]
        taskDescription = json.loads(row[2])
        reqs = json.loads(row[1])
        freq = solved[taskid]/m
        node_id = "g_node_{}".format(taskid)
        tooltip = """{}
ID: {}
Title: {}
Solved: {} times""".format(taskDescription['shortname'], taskid, taskDescription['title'], solved[taskid])
        # color = hsluv_to_hex([360, 100, 100-floor(freq*50)])
        color = cmap[floor(freq*(len(cmap)-1))]
        dot.node(str(taskid), "{}".format(taskid), id=node_id, fillcolor=color, tooltip=tooltip)
        node_ids.append({"id": node_id, "taskid": taskid})
        
        arrowsize = str(floor(7 - 6*freq*freq))
        #print(arrowsize, 100-floor(freq*50))
        penwidth = str(floor(1+25*freq))
        for req in reqs:
            if type(req) == type(1):
                r = req
                #print(r, solved[r], taskid, solved[taskid], )
                edge_freq = solved[taskid]/solved[r]
                edge_freq = edge_freq * edge_freq
                arrowsize = str(floor(7 - 6*edge_freq*edge_freq))
                penwidth = str(floor(2+25*edge_freq))
                dot.edge(str(r), str(taskid), penwidth=penwidth, arrowsize=arrowsize)
            elif type(req)  == type([]):
                for r in req:
                    if type(r)  == type(1):
                        #print(r, solved[r], taskid, solved[taskid], solved[taskid]/solved[r])
                        edge_freq = solved[taskid]/solved[r]
                        edge_freq = edge_freq * edge_freq
                        arrowsize = str(floor(7 - 6*edge_freq*edge_freq))
                        penwidth = str(floor(2+25*edge_freq))
                        dot.edge(str(r), str(taskid), penwidth=penwidth, arrowsize=arrowsize) # str(floor(1+50*freq))
                    else:
                        print("error, type is: " + type(r))
            else:
                print("error, type is: " + type(req))
    # TODO uncomment the next line or the graph will not be rendered!
    #path = dot.render(filepath+"graph.gv", format="svg")
    #print(path)
    # static inclusion of precomputed svg graph-object, if line above is not commented, change filename to graph.gv.svg
    path = "pages/svg/graph_static.gv.svg"
    with open(path, 'r') as file:
        return file.read(), node_ids

def freqSolved():
    successfulSubmissions = getData(sqlquery = 'SELECT DISTINCT taskid, user FROM submissions WHERE score == 1.0')
    solved = {}
    m = 0
    for s in successfulSubmissions:
        solved[s[0]] = 0
    for s in successfulSubmissions:
        solved[s[0]] += 1
        if solved[s[0]] > m:
            m = solved[s[0]]
    return m, solved

if __name__ == "__main__":
    #print(examplegraph())
    print(constructGraph()[1])


