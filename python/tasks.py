#!/usr/bin/python
try:
    from pages.data import getData, getTable, getTags, getColorDict
except ModuleNotFoundError:
    import sys
    sys.path.append("./pages")
    from data import getData, getColorDict
import json
import pandas as pd
import plotly.express as px
#import plotly.io as pio
#pio.templates.default = 'ggplot2'

def preprocessData(df):
    df_long = df[['user', 'taskid','resultType','date']].groupby(['taskid','resultType'], as_index=False).count()
    df_wide = df_long.pivot(index = ['taskid'], columns = ['resultType'], values = 'date')
    df_wide.fillna(0, inplace=True)
    df_wide.loc[:,'Total'] = df_wide.sum(axis=1)

    df_wide = df_wide.sort_values(by='Total', ascending=False)
    return df_wide


def explore(df, callPlot = False):
    #############
    # remove timestamp while not used!!!
    del df['timestamp']
    del df['date']
    #############
    df_grouped = df.groupby(['taskid', 'resultType'], as_index=False).count()
    if callPlot:
        print(df)
        print(df_grouped)

    fig = px.histogram(df_grouped, y='taskid', x='user', color='resultType',
            labels={'taskid':'Aufgaben IDs','user':'submissions (by task)'
            },
            color_discrete_map=getColorDict(['SUCCESS','WRONG_ANSWER','RUN_ERROR']),
            hover_name='taskid'
            )
    nog = df_grouped.shape[0]
    fig.update_yaxes(categoryorder='total ascending')
    fig.update_layout(height=7.5*nog, 
            legend=dict(yanchor="top",y=0.99, xanchor="right", x=1.19),
            xaxis={'side': 'top'},
    )

    #add quantile
    df_success = df[df['resultType'] == 'SUCCESS']
    df_success = df_grouped[df_grouped['resultType'] == 'SUCCESS']
    df_wrong = df_grouped[df_grouped['resultType'] == 'WRONG_ANSWER']
    df_runerror = df_grouped[df_grouped['resultType'] == 'RUN_ERROR']
    df_all = df.groupby(['taskid'], as_index=False).count()

    if callPlot:
        fig.show()
        return 
    return fig
    
def preprocessTimePlotData(df, resultTypes = ['SUCCESS','WRONG_ANSWER','RUN_ERROR']):
    # drop columns not in resultTypes
    df = df[df['resultType'].isin(resultTypes)]
    df_grouped = df.groupby(['taskid'],as_index=False)
    for _ , group in df_grouped:
        df.loc[group.index,'q25'] = group['date'].quantile(.25)

    # sort by median-col
    df = df.sort_values(by='q25', ascending=False)
    return df, df_grouped.ngroups

def getTimePlot(df, resultTypes = ['SUCCESS','WRONG_ANSWER','RUN_ERROR'], callPlot = False):
    df, nog = preprocessTimePlotData(df=df, resultTypes=resultTypes)
    fig = px.box(df, y='taskid',x='date', points=False,
            labels={'taskid':'Aufgaben IDs','date':'Time of Submission'},
            color_discrete_map=getColorDict(['Time']))
    fig.update_traces(orientation='h', 
            jitter=0.2,
            whiskerwidth=0.2,
            fillcolor='#636EFA',
            marker_color='#A3AEFF',
            marker_size=2,
            line_width=1
    )
    fig.update_layout(hovermode='y unified',
            xaxis={'side': 'top'}
    )
    if callPlot:
        fig.show()
    return fig

def getDropdownTaskidOptions():
    sqlquery = "select distinct taskid, taskDescription from tasks where lang == 'python'"
    df = getData(sqlquery = sqlquery)
    options = [{'label': '{}: {}'.format(taskid, json.loads(taskDesc)['shortname'][:25]), 'value': taskid} for taskid, taskDesc in zip(df['taskid'], df['taskDescription'])]
    return options

def getTaskPlot(taskid, plotType = 0, callPlot = False):
    sqlquery = "select submissions.taskid, submissions.resultType, submissions.user, submissions.timestamp \
        from submissions \
        where submissions.course == 'propäd' and submissions.taskid = {} \
        order by submissions.user, submissions.timestamp asc".format(taskid)
    df = getData(sqlquery = sqlquery)
    del df['taskid']
    del df['date']

    df = df.assign(count1 = 1)
    if plotType == "triesTillSuccess":
        # prepare dataframe for boxplot-triesTillSuccess
        df_wide = pd.pivot_table(df, index='user', columns='resultType', values='count1', aggfunc='sum', fill_value=0).reset_index()
        print(df_wide)
        if not 'SUCCESS' in df_wide.columns:
            df_wide = df_wide.assign(SUCCESS = 0)
        if not 'RUN_ERROR' in df_wide.columns:
            df_wide = df_wide.assign(RUN_ERROR = 0)
        if not 'WRONG_ANSWER' in df_wide.columns:
            df_wide = df_wide.assign(WRONG_ANSWER = 0)
        df_wide['TOTAL'] = df_wide[['RUN_ERROR', 'SUCCESS', 'WRONG_ANSWER']].sum(axis=1)
        # FIGURE: triesTillSuccess
        y = 'TOTAL'
        fig = px.box(df_wide, y=y, points="all",
                labels={y: 'Number of Tries'},
                color_discrete_map=getColorDict(['Count']))
        fig.update_layout(
            legend=dict(
                title=None, orientation="h", y=1, yanchor="bottom", x=0.5, xanchor="center"
            )
        )
        fig.update_xaxes(title_text=y)

    if plotType == "timeTillSuccess":
        # prepare dataframe for boxplot-timeTillSuccess
        df_duration = pd.DataFrame(columns = ['user', 'duration'])
        df_grouped = df.groupby(['user'])
        for key, group in df_grouped:
            umin_ts = group['timestamp'].loc[group['timestamp'].idxmin()] 
            umax_ts = group['timestamp'].loc[group['timestamp'].idxmax()] 
            user_td = umax_ts - umin_ts
            df_duration.loc[len(df_duration.index)] = [key, user_td.total_seconds()//60]

        # FIGURE: timeTillSuccess
        fig = px.box(df_duration, y='duration',log_y=True,
                color_discrete_map=getColorDict(['Time']))
        #fig_duration.update_yaxes(range=[-1,df_duration['duration'].quantile(.9)])

    
    if plotType == 0:
        del df['timestamp']
        fig = px.histogram(df, x='resultType', y='user')
    
    if callPlot:
        fig.show()
    return fig

def getTaskInformation(taskid):
    sqlquery = "select taskDescription from tasks where taskid == {}".format(taskid)
    taskString = getData(sqlquery = sqlquery)['taskDescription'][0]
    taskDict = json.loads(taskString)
    sqlquery = "select distinct submissions.user from submissions where taskid == {}".format(taskid)
    taskUser = getData(sqlquery = sqlquery)
    tags = list(getTags(taskid).keys())
    print(taskUser.shape[0]) #Number of users who tried the task
    return [taskDict['title'],tags, taskDict['task']]

def getTaskSubmissions(taskid):
    sqlquery = "select distinct content, resultType, count(content) as duplicates from submissions \
        where taskid = {} \
        group by content, resultType \
        order by duplicates desc".format(taskid)
    submissions = getData(sqlquery = sqlquery)
    sqlquery = "select solution from tasks where taskid = {}".format(taskid)
    solution = getData(sqlquery = sqlquery)
    submissions['content'].astype(str)
    return [submissions, solution.loc[0]]

if __name__ == "__main__":
    print("eexcute main of python/tasks")
    df = getData('./sqlqueries/submissions_all', isFilepath=True)
    #df = expandDataframe(df)

    callPlot = True
    plotType = "triesTillSuccess"
    #explore(df, callPlot = callPlot)
    getTimePlot(df, callPlot = callPlot)
    #getTaskPlot(1209, plotType = plotType, callPlot = callPlot)

