#!/usr/bin/python
try:
    from pages.data import getData, getColorDict
except ModuleNotFoundError:
    import sys
    sys.path.append("./pages")
    from data import getData, getColorDict

import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

def preprocessData(df, slider=False, callPlot=False):
    if not slider:
        del df['timestamp']
        del df['date']
    df_long = df[['user', 'taskid','resultType']].groupby(['user','resultType'], as_index=False).count()
    df_wide = df_long.pivot(index = 'user', columns = 'resultType', values = 'taskid')
    df_wide.loc[:,'Total'] = df_wide.sum(axis=1)
    df_wide.fillna(0, inplace=True)

    if callPlot:
        print(df_long)
        print(df_wide)

    return df_wide

def getPlot(df, resultTypes=['SUCCESS', 'WRONG_ANSWER', 'RUN_ERROR'], 
        showALL=True, sortby='Total', slider=False, callPlot = False):
    df_wide = preprocessData(df, slider=slider, callPlot=callPlot)
    df_wide = df_wide.sort_values(by=sortby, ascending=False)

    # create figure
    fig = px.bar(df_wide, y=resultTypes,
            labels={'user':'User ID', 'value':'Number of Submissions', 'variable':'Result Type'},
            color_discrete_map=getColorDict(resultTypes))
    fig.update_traces(width=0.9)
    if showALL:
        fig.add_trace(go.Scatter(x=list(df_wide.index), y=df_wide['Total'], 
            name="SUM per User", line_shape='hvh', 
            line_color=getColorDict(['Total'])['Total'], line_width=1))
    # layout hover
    fig.update_traces(
        #hovertemplate x unified
        #hovertemplate = 
        #    '<b>: %{y}</b>'+
        #    '<br><i>UserID: %{x}</i>'
        hovertemplate = '<b>Count: %{y}</b>'
    )
    fig.update_layout(
        hovermode=False, #'x',
        hoverlabel=dict(
            bgcolor="#efecec",
            bordercolor="#dfdcdc",
            font_size=14,
            font_color='black',
            #grouptitlefont=dict(size=14),
        ),
        font_size=16
    )
    fig.update_xaxes(showspikes=True, spikemode='across', spikesnap='hovered data',
            spikethickness=1, spikecolor="grey",
            tickfont_size=10,
            showticklabels=False)
    fig.update_yaxes(showspikes=True, spikemode='across+marker', 
            spikesnap='hovered data', spikecolor="grey", spikethickness=1,
            rangemode='nonnegative')
    fig.update_layout(spikedistance=50)
        
    # add quartile traces
    qxx = [.1,.25,.5,.75,.9]
    quantiles = []
    if len(resultTypes) == 1: 
        resultType = resultTypes[0]
        quantiles = [df_wide[resultType].quantile(q) for q in qxx]
    elif showALL: 
        quantiles = [df_wide['Total'].quantile(q) for q in qxx]
    for q, quantile in zip(qxx,quantiles):
        if q != 0.5:
            fig.add_hline(y=quantile, opacity=0.5, line_color='white',
                annotation_text="{}%-Quantile: <b>{:.2f}</b>".format(int(q*100), quantile), 
                annotation_position="top right", annotation_font_size=14,
            )
    if len(quantiles) > 0:
        nuser = df_wide.shape[0]
        fig.add_vrect(x0=-0.5, x1=qxx[0]*nuser-0.5, line_width=0, fillcolor="grey", opacity=0.2)
        fig.add_vrect(x0=-0.5, x1=qxx[1]*nuser-0.5, line_width=0, fillcolor="grey", opacity=0.2)
        fig.add_vrect(x0=qxx[-2]*nuser-0.5, x1=nuser, line_width=0, fillcolor="grey", opacity=0.2)
        fig.add_vrect(x0=qxx[-1]*nuser-0.5, x1=nuser, line_width=0, fillcolor="grey", opacity=0.2)
        fig.add_hline(y=quantiles[2],
                annotation_text="<b>50%-Quantile: {:.2f}</b>".format(quantile),
                annotation_position="top right", annotation_font_size=16,
                )
    
    if callPlot:
        fig.show()

    return fig
    


if __name__ == "__main__":
    print("execute main of python/users")
    df_user = getData("./sqlqueries/submissions_peruser", isFilepath=True)
    resultTypes=['SUCCESS','WRONG_ANSWER','RUN_ERROR']
    showALL=True
    #resultTypes=['SUCCESS']
    #showALL=False

    callPlot = True
    getPlot(df_user, resultTypes, showALL=showALL, callPlot=callPlot)



