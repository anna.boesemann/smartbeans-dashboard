from content import app

server = app.server
if __name__ == "__main__":
    app.run_server("0.0.0.0", 8000)
