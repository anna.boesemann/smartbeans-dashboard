from index import app
from python import tasks as task
from pages.data import getData, getTags
import pandas as pd
from dash import Dash, State, dcc, dash_table, html
import dash_bootstrap_components as dbc
import plotly.graph_objects as go
#import plotly.io as pio
#pio.templates.default = "ggplot2"

# ======================== Functions for components default content
def getFilterOptions():
    print("getFilterOptions:")
    tags_index = getTags()
    options = list(tags_index.keys())
    return options

def conditionals(df, isTime=False):
    styles=[{   
                    'if': {'column_id': 'taskid'},
                    'color': 'black',
                    'fontWeight': 'bold'
                }]
    if isTime:
        conditional2 =[{
                    'if': {'filter_query': '{{date}} = {}'.format(i)},
                    'backgroundColor': '#efecec'
                 } for i in df['date'].nlargest(3)]
        conditional3 =[{
                    'if': {'filter_query': '{{date}} = {}'.format(i)},
                    'backgroundColor': '#efecec'
                 } for i in df['date'].nsmallest(3)]
    else:
        conditional2 =[{
                    'if': {'filter_query': '{{Total}} = {}'.format(i)},
                    'backgroundColor': '#efecec'
                 } for i in df['Total'].nlargest(3)]
        conditional3 =[{
                    'if': {'filter_query': '{{Total}} = {}'.format(i)},
                    'backgroundColor': '#efecec'
                 } for i in df['Total'].nsmallest(3)]

    styles += conditional2 + conditional3
    return styles


# ======================== App Layout
cardStyle = {
    'padding':'10px',
    'margin-top':'5px',
    'margin-bottom':'5px',
    'margin-left': "0px",
    'margin-right': "0px",
}

def getTaskLeftInformation():
    header = dbc.ModalHeader(dbc.ModalTitle("Explore Tasks"))
    markdown = dcc.Markdown('''Here you can explore the courses' tasks.

The left side shows either the *number of submitted solutions* or the *typical dates/times of solving* per task, respectively.

On the right-hand side you can see details for a selected task. You can select a task from the dropdown menu above or click on a graph in one of the plots from the left side:

- Plot: Number of tries until the task is successfully solved (average over all students)
- Plot: Time between the first try and until the task is successfully solved (average over all students)
- Task description and tags
- Most frequent submissions and their respective results

''')
    body = dbc.ModalBody(markdown)
    return [header, body]

getTaskRightInformation = getTaskLeftInformation

def getTagBadge(name):
    print("return tag badges")
    return dbc.Badge(name, 
            color="secondary", text_color="light",
            className="border me-1",
    )

def getFigureCard():
    title = html.H3(id='task-plot-title')
    graph = html.Div(
                dcc.Graph(id='fig_tasks'),
                style={'overflowY': 'scroll', 'height': '70vh'},
                id='graph-div'
            )
    daterange = pd.date_range(start ='10-11-2021', end='10-17-2021', freq='3H')
    slider = html.Div(dcc.RangeSlider(id='date-slider-tasks',
                min=0,#pd.Timestamp('2021-10-11'), # Monday 
                max=6*8+1,#pd.Timestamp('2021-10-17'), # Sunday
                value=[2,4*8+1],#[pd.Timestamp('2021-10-12'),pd.Timestamp('2021-10-15')], # Monday to Sunday
                step = 1,
                marks = {(date.weekday()*8+(date.hour//3)):date.day_name()+" 12h"  
                    for date in daterange if date.hour == 12},
            ),style={'margin-top':'10px'}
            )
    print("return FigureCard")
    return dbc.Card([title, graph, slider],
            class_name="card-1",
            body=True,
            style=cardStyle)

def getTableCard():
    tableCard = dbc.Card([
        html.H4(children='Details'),
        html.P("Select a task from above or the plots on the left."),
        dash_table.DataTable(
            id='datatable_tasks',
            page_size=100,
            fixed_rows={'headers': True},
            style_cell={'textAlign': 'left'},
            style_table={'overflowY': 'auto'},
            style_header={
                'backgroundColor': '#efecec',
                'fontWeight': 'bold'
            },
        )],
        class_name="card-1",
        body=True,
        style=cardStyle
    )
    print("return tableCard")
    return tableCard

def getTaskLayout():
    taskCard = dbc.Card([
        html.H3(id='task-title',
            children='Task title placeholder',
        ),
        dbc.Row([
            dbc.Col(dcc.Dropdown(id="task-dropdown-plotselector",
                options=['Tries until success', 'Time until success'],
                value='Tries until success',
                clearable=False
            ), width=3),
            dbc.Col(html.P(id='taskid-p-element',
                children='TaskID: -'), style={"visibility": "hidden"}),
            dbc.Col(html.Div(id='taskcompetencies-div-element',
                children="", style={"margin-left": 'auto'}), width=3),
        ]),
        dcc.Graph(id='fig_task',
            figure=go.Figure()
            ),
        html.Div([
            dbc.Button("Show task",
                id="btn-collapse-taskdescription",
                className="mb-4",
                color="primary",
                n_clicks=0,
            ),
            dbc.Collapse(dcc.Markdown(id='taskDescription',
                children='Here appears the task description.'),
                id="collapse-taskdescription",
                is_open=False
            )],
            style={'margin':'10px','background-color': '#efecec', 'padding': '10px'}
            ),
        #dash_table.DataTable(
        #    id='task_submissions',
        #    page_size=20,
        #    fixed_rows={'headers': True},
        #    style_cell={
        #        'overflow': 'hidden',
        #        'textOverflow': 'ellipsis',
        #        'maxWidth': 0
        #    },
        #    #tooltip_data
        #    tooltip_duration=None
        #)
        ],
        style=cardStyle)
        #style={'padding':'10px'})
    print("return taskCard")
    return taskCard

def getLayoutLeft():
    print("return layoutLeft")
    return [getFigureCard()] 

def getLayoutRight(taskSelected = False):
    print("taskselected: ", taskSelected)
    if taskSelected:
        return getTaskLayout()
    else: 
        return getTableCard()

def getControlsLeft():
    controls = dbc.Row([
                dbc.Col(dcc.Dropdown(id="dropdown_plotselector",
                    options=['Number of Submissions', 'Task completion time'],
                    value='Number of Submissions',
                    clearable=False
                ), width=4),
                dbc.Col(dcc.Dropdown(id='graph-filter-dropdown',
                    options = getFilterOptions(),
                    placeholder="Select tags to filter...",
                    multi=True
                )),
                dbc.Col(dbc.Button('Full Screen', 
                    id='btn-fullscreen', 
                    n_clicks=0, 
                    color="primary"), width=2),
                dbc.Col(dbc.Button('i',
                    id='btn-task-left-information',
                    color="primary",
                    ),
                    width={"size":1, "order":"last"}),
            ], align="center")
    print("return controls left")
    return dbc.Card(controls, style=cardStyle)

def getControlsRight():
    controls = dbc.Row([
                dbc.Col(html.Span("Select a task"), width=2),
                dbc.Col([dcc.Dropdown(id="dropdown-taskid",
                    placeholder="Select a taskid ...",
                    options=task.getDropdownTaskidOptions(),
                    )
                ]),
                dbc.Col(dbc.Button("Reset task",
                        id="btn-show-table", 
                        color="primary"), 
                    width={"size": 2, "order": "last"}),
                dbc.Col(dbc.Button('i',
                    id='btn-task-right-information',
                    color="primary",
                    #style={'padding-left':'5px','padding-right':'5px'}
                    ),
                    width={"size": 1, "order": "last"}),
            ], align="center", justify="start")
    print("return controls right")
    return dbc.Card(controls, style=cardStyle)

def tasks():
    print("called Tab: tasks()")
    layout = html.Div([
        dbc.Row([
            dbc.Col(class_name="col-6",
                children=getControlsLeft()),
            dbc.Col(class_name="col-6",
                children=getControlsRight())
        ]),
        dbc.Row([
            dbc.Col(id="col-tasks-left",
                children=getLayoutLeft(),
                class_name="col-6"
            ),
            dbc.Col(id="col-tasks-right",
                #children=getLayoutRight(),
                class_name="col-6"),
            ],
            class_name="row-2",
            #style={"height": "92vh"},
        ),
        dbc.Modal(
            [
                dbc.ModalHeader(dbc.ModalTitle("Header", id="modal-title")),
                dbc.ModalBody("An extra large modal.",
                    id="model-content"),
            ],
            id="modal-xl",
            size="xl",
            fullscreen=True,
            is_open=False,
        ),
        dbc.Modal(getTaskLeftInformation(),
            id="tasks-left-information",
            size="lg",
            is_open=True,
        ),
        dbc.Modal(getTaskRightInformation(),
            id="tasks-right-information",
            size="lg",
            is_open=False,
        ),
        ],
        style={'padding':'5px', 'background-color': '#a8d3ed'}, #ede9e8
        className="tab",
    )
    print("return tab layout")
    return layout
