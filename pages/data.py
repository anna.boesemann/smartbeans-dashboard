#!/usr/bin/python
import sqlite3
import json
import pandas as pd
from datetime import datetime
import plotly.express.colors as pxc

db_file = "./db.sqlite"

def dropSubmissionsAfterSuccess(df):
    df_grouped = df.groupby(['user','taskid'], as_index=False)
    drop_indexlist = []
    for key, group in df_grouped:
        x1 = group['resultType'].eq('SUCCESS').idxmax()
        x1_ts = group.loc[x1]['timestamp']
        mask = group['timestamp'] > x1_ts
        indexlist = mask[mask == True]
        drop_indexlist += list(indexlist.index)
    df = df.drop(index=drop_indexlist)
    return df

def getData(sqlquery, isFilepath = False, db_file = "./db.sqlite"):
    print("Executing sqlquery...")
    #open database
    conn = sqlite3.connect(db_file)

    if isFilepath:
        filepath = sqlquery
        with open(filepath,"r") as f:
            sqlquery = f.read()
    cursor = conn.execute(sqlquery)

    # make dataframe from sqlquery output
    column_names = [description[0] for description in cursor.description]
    df = pd.DataFrame(cursor.fetchall(), columns = column_names)

    #close database connection
    conn.close()

    # format data
    if 'taskid' in df.columns:
        # interpret taskid as string
        df['taskid'] = df['taskid'].astype(str)
    if 'user' in df.columns:
        # interpret user(id) as string
        df['user'] = df['user'].astype(str)
    if 'timestamp' in df.columns:
        # transform timestamp to datetime
        df['timestamp'] = df['timestamp'].apply(lambda x: datetime.fromtimestamp(x/1000))
        df['date'] = df['timestamp'].astype('datetime64[ns]')
        from_ts = pd.Timestamp(2021,10,11,0,0,0)
        to_ts = pd.Timestamp(2021,10,17,0,0,0)
        df = df[(df['date'] >= from_ts) & (df['date'] <= to_ts)]
    # drop submissions after first success
    if 'taskid' in df.columns and 'user' in df.columns and 'timestamp' in df.columns:
        df = dropSubmissionsAfterSuccess(df)
    else:
        print("CAUTION: \n Submissions after first SUCCESS not dropped. This might be unwanted behavior")
    return df

def getTable(table_name, isFilepath = False):
    print("getTable")
    sqlquery = "select * from {}".format(table_name)
    return getData(sqlquery = sqlquery, isFilepath = isFilepath)

def getColumn(col, table_name = "submissions", distinct = True, isFilepath = False):
    print("getColumn")
    if type(col) is not str:
        print("Only supports one column. Use getData(sqlquery) with a valid sqlquery for more complex requests.")
    distinct_str = ""
    if distinct:
        distinct_str = "distinct"
    sqlquery = "select {} {} from {}".format(distinct_str, col, table_name)
    return getData(sqlquery = sqlquery, isFilepath = isFilepath)

def getTags(taskid = None):
    print("getTags")
    sqlquery = "select taskid, tags from courseTask where course == 'propäd'"
    if taskid:
        sqlquery += " and taskid == {}".format(taskid)
        df_raw = getData(sqlquery = sqlquery)
    else: 
        df_raw = getData(sqlquery = sqlquery)

    #extract available tags and create "index"
    tags = {}
    for taglist, task in zip(df_raw['tags'],df_raw['taskid']):
        my_dict = json.loads(taglist)
        for d in my_dict:
            if 'points' in d:
                if taskid:
                    tags[d['name']] = d['points'] 
                else:
                    try: 
                        tags[d['name']].append(task)
                    except:
                        tags[d['name']] = [task]
    return tags

def getColorDict(keys=['Total','SUCCESS','RUN_ERROR','WRONG_ANSWER']):
    plotlycolor = pxc.qualitative.Plotly
    fullDict = {'Total': '#111111',
            'SUCCESS': plotlycolor[2],
            'RUN_ERROR': plotlycolor[4],
            'WRONG_ANSWER': plotlycolor[1],
            'Time': plotlycolor[0],
            'Count': plotlycolor[3]
            }
    colorDict = {key:color for key,color in fullDict.items() if key in keys}
    return fullDict
