from index import app
from pages.data import getData, getColorDict
from dash import dcc, html
import dash_bootstrap_components as dbc
import plotly.express as px

overviewTabStyle = {
    'padding':'5px',
    'background-color':'#a8d3ed'
}

cardStyle = {
    'padding':'20px',
    'margin-top':'5px',
    'margin-bottom':'5px',
    'margin-left': "0px",
    'margin-right': "0px",
    'height':'44vh', # tabs 100vh-6vh ->  
    'max-height':'46vh',
    'width':'48vw'
}

def getSubmissionCount(df=None, animate=False):
    try: 
        return df['count'].sum()
    except: 
        return 0

def getFigureCard1(df=None, animate=False):
    if df is None:
        if animate:
            sqlquery = "select course, resultType, \
            date(datetime(cast(submissions.timestamp/1000 as integer), \
            'unixepoch', 'localtime')) as daygroup, count(id) as 'count' \
            from submissions \
            where course == 'propäd' and daygroup < datetime('2021-10-17') \
            group by course, resultType, daygroup \
            order by daygroup"
        else:
            sqlquery = "select course ,resultType, count(*) as 'count' \
                from submissions where course == 'propäd' \
                group by course, resultType"
        df = getData(sqlquery = sqlquery)
    hover_data = ['count']
    if 'percentage' in df.columns:
        hover_data = ['count','percentage']
    count = getSubmissionCount(df)
    xmax = count
    if animate:
        df['cumsum'] = df[['course', 'resultType', 'count']].groupby('resultType').cumsum()
        fig = px.bar(df, y='course', x='cumsum', 
            color_discrete_map=getColorDict(['SUCCESS', 'WRONG_ANSWER','RUN_ERROR']),
            animation_frame="daygroup", animation_group="resultType",
            range_x=[0,xmax], color='resultType',
            hover_name="resultType", hover_data=hover_data
            )
        fig.update_layout(transition = {'duration': 10000})
    else:
        fig = px.bar(df, y='course', x='count', 
            color='resultType',
            color_discrete_map=getColorDict(['SUCCESS', 'WRONG_ANSWER','RUN_ERROR']),
            hover_name="resultType", hover_data=hover_data
            )

    fig.update_traces(
            width=0.5,
            texttemplate="%{x}",
            textposition="inside",
            textfont_color="white",
            textfont_size=20
    )
    fig.update_yaxes(title=None, showline=False, 
            showgrid=False, showticklabels=False)
    fig.update_xaxes(title=None)
    fig.update_layout(showlegend=False,
        #autosize=False,
        height=250,
        margin=dict(l=0,r=0,b=0,t=0,pad=4),
        #hovermode="x unified",
        hoverlabel=dict(
            bgcolor='#efecec',
            font_size=16,
        ),
    )
    return fig

def getUserGuide():
    title = dbc.Row(html.H2('About this dashboard and its data'), justify="center", align="center")
    markdown = dbc.Row(dcc.Markdown('''The Informatik Propädeutikum took place for the first time in 2021. It's target audience are bachelor freshmen from Göttingens computer science programs. The second weeks topic is python programming. Students are learning the basics self-directedly with an online system, the [SmartBeans](https://gitlab.gwdg.de/smart/smartbeans), originally developed at Uni Göttingen for a C programming course. 

This dashboard makes the course data from 2021 available and explorable. On the next tabs you can explore different aspects of this data like tasks, submitted solutions, learning behaviour and the prerequisites structure.
You can usually interact with plots and hover, zoom or select data. All student data is handled pseudonymously.
'''))
    return [title, markdown]


def getCard1(cardid = 1, animate = False):
    title="Submission Counter"
    if animate:
        title += "(Live Simulation by Day)"
    if cardid == 1:
        submissionCount = dbc.Col(html.P(id="card{}-p-element".format(cardid), 
            children=getSubmissionCount(animate=animate),
                    style={'margin':'10px', 'padding':'15px',
                        #'background-color':'#77badb',
                        'width':'fit-content', 'font-size':'5vh'}
                    ),
                    width=3,
                )
    else:
        submissionCount = dbc.Col(width=1)
    return [dbc.Row(html.H3(title,
                style={'padding':'5px', 'text-align':'center'}),
            ),
            dbc.Row([
                dbc.Col(dcc.Graph(id="card{}-graph".format(cardid),
                    figure=getFigureCard1(animate=animate),
                    config={'displayModeBar':False},
                    #style={'width':'70%'}
                    ),
                ),
                submissionCount,
            ], 
            align="center", justify="start",
            style={'height':'40vh'}
            ),
            dbc.Row([
                dbc.Col(dbc.Button("↻",
                        #html.I(className="bi bi-arrow-clockwise"),
                        id="btn-update-card{}".format(cardid), 
                        style = {'width':'fit-content', 
                            'padding-left':'20px', 'padding-right':'20px'},
                        color="primary", n_clicks=0
                    ),
                    width=1,
                    align="end",
                )],
                align="end",
                justify="end",
            ),
        ]

def getCard2():
    return getUserGuide()

def getCard4():
    return dbc.Col([
        dbc.Row(html.H2('What students are currently working on'), justify="center", align="center"),
        dbc.Row(dcc.Graph(id="card4-graph", config={'displayModeBar':False} ))
    ])

def overview():
    print("Called Tab: Overview()")
    return html.Div([
        dbc.Row([
            dbc.Col(dbc.Card(
                children=getCard1(animate=False),
                id="card1",
                style=cardStyle
            )),
            dbc.Col(dbc.Card(
                children=getCard2(),
                id="card2",
                style=cardStyle
            )),
        ], align="center"),
        dbc.Row([
            dbc.Col(dbc.Card(
                children=getCard1(cardid=3, animate=True),
                id="card3",
                style=cardStyle
            )),
            dbc.Col(dbc.Card(
                children=getCard4(),
                id="card4",
                style=cardStyle
            )),
        ], align="center"),
        dbc.Modal(
            [
                dbc.ModalHeader(dbc.ModalTitle("Usage Information")),
                dbc.ModalBody(id="modal-overview-content", children="A large modal."),
            ],
            id="modal-overview",
            size="lg",
            is_open=False,
        ),
    ],
    style=overviewTabStyle
    )
