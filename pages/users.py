from index import app
from python import users
from dash import Dash, dcc, dash_table, html
import dash_bootstrap_components as dbc

cardStyle = {
    'padding':'5px',
    'margin-top':'5px',
    'margin-bottom':'5px',
    'margin-left': "0px",
    'margin-right': "0px",
}

def getUserTabInformation():
    header = dbc.ModalHeader(dbc.ModalTitle("User Progress: Submissions"))
    markdown = dcc.Markdown('''"How well do different students cope with the material?" is an important question every course instructor asks himself/herself. Here it is best answered by looking at **how many tasks each student solved in a sorted bar chart**.

We can look e.g.:

- Sudden drops in the curve which could indicate students getting left behind.
    - *Sort by* SUCCESS, *Filter* SUCCESS
- How many tasks were solved by 50% / 75% / 90% of students.
- How do successful / wrong submissions correlate with each other?
    - e.g. *Sort by* SUCCESS, *Filter* WRONG_ANSWER
''')
    body = dbc.ModalBody(markdown)
    return [header, body]

def getControls():
    controls = dbc.Row([
            dbc.Col(html.H4(children='Submissions of each user')),
            dbc.Col(html.Label("Sort by:", style={"display": "inline-block", "width": "150px", "text-align": "center"}), width=1),
            dbc.Col(dcc.Dropdown(id="dropdown-sortselector",
                    options = ['Total','SUCCESS','WRONG_ANSWER','RUN_ERROR'],
                    value='Total',
                    clearable=False
            ), width=2),
            dbc.Col(html.Label("Filter type:", style={"display": "inline-block", "width": "150px", "text-align": "center"}), width=1),
            dbc.Col(dcc.Dropdown(id='usergraph-filter-dropdown',
                    options = ['Total','SUCCESS','WRONG_ANSWER','RUN_ERROR'],
                    value = ['Total','SUCCESS','WRONG_ANSWER','RUN_ERROR'],
                    placeholder="Select resultTypes to filter...",
                    multi=True
                ), width=4),
            dbc.Col(dbc.Button('i',
                id='btn-user-information',
                color="primary",
                ), width=1),
            ],
            align="center",
            justify="between"
        )
    return dbc.Card(controls, style=cardStyle)

def getLayoutUser():
    return dbc.Card([
            dcc.Graph(id='user-graph'),
            ],
            class_name="card-2",
            style=cardStyle,
            body=True
        )

def users():
    print("Called Tab: users()")
    return html.Div([
        dbc.Row([
            dbc.Col(class_name="col-12",
                children=getControls()),
        ]),
        dbc.Row([
            dbc.Col(id="col-user-content",
                children=getLayoutUser(),
                class_name="col-12"
            )],
            class_name="row-2",
            #style={"height":"92vh"},
        ),
        dbc.Modal(getUserTabInformation(),
            id="user-information",
            size="lg",
            is_open=True,
        ),
        ],
        style={'padding':'5px', 'background-color': '#a8d3ed'}, #ede9e8
        className="tab",
    )
