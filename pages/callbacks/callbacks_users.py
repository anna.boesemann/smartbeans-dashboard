from index import app
from pages.users import users
from pages.data import getData
from python.users import getPlot, preprocessData
from dash import Input, Output, State
import pandas as pd

@app.callback(
    Output('user-graph', 'figure'),
    Input('df_users_storage','data'),
    Input('dropdown-sortselector', 'value'),
    Input('usergraph-filter-dropdown','value'))
def update_userGraph(df_asjson, sortby,filterlist):
    if df_asjson is None:
        df_user = getData("./sqlqueries/submissions_peruser", isFilepath=True)
    else:
        df_user = pd.read_json(df_asjson)
        df_user['taskid'] = df_user['taskid'].astype(str)
        df_user['user'] = df_user['user'].astype(str)
    showALL = False
    if 'Total' in filterlist:
        showALL = True
        filterlist.remove('Total')

    resultTypes = filterlist
    slider=True
    fig = getPlot(df_user, resultTypes=resultTypes, showALL=showALL, sortby=sortby, slider=slider)
    fig.update_layout(height=700)
    return fig

@app.callback(
    Output("user-information", "is_open"),
    Input('btn-user-information', 'n_clicks'),
    State("user-information", "is_open"),
    prevent_initial_call=True)
def openTaskRightInformation(n_clicks, is_open):
    if n_clicks:
        return not is_open
    return is_open

