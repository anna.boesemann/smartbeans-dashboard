from index import app
from pages.data import getData
from pages.overview import overview, getSubmissionCount, getFigureCard1
from python.tasks import getTimePlot
from dash import Input, Output, ctx
from dash.exceptions import PreventUpdate
from python import tasks as task

import pandas as pd
import plotly.express as px
#import plotly.io as pio
#pio.templates.default = "ggplot2"

@app.callback(
    Output('card1-p-element', 'children'),
    Output('card1-graph','figure'),
    Input('btn-update-card1', 'n_clicks'))
def update_card1(n_clicks):
    sqlquery = "select course ,resultType, count(*) as 'count' \
            from submissions where course == 'propäd' \
            group by course, resultType"
    df = getData(sqlquery = sqlquery)
    df['percentage'] = df['count'].apply(lambda c: 100*c/df['count'].sum())
    submissionCount = getSubmissionCount(df)
    fig = getFigureCard1(df, animate=False)
    return [submissionCount, fig]

@app.callback(
    Output('card3-p-element', 'children'),
    Output('card3-graph','figure'),
    Input('btn-update-card3', 'n_clicks'))
def update_card3(n_clicks):
    sqlquery = "select course, resultType, \
            date(datetime(cast(submissions.timestamp/1000 as integer), \
            'unixepoch', 'localtime')) as daygroup, count(id) as 'count' \
            from submissions \
            where course == 'propäd' and daygroup < datetime('2021-10-17') \
            group by course, resultType, daygroup \
            order by daygroup"
    df = getData(sqlquery = sqlquery)
    df['percentage'] = df['count'].apply(lambda c: 100*c/df['count'].sum())
    submissionCount = getSubmissionCount(df)
    fig = getFigureCard1(df, animate=True)
    return [submissionCount, fig]


@app.callback(
    Output('card4-graph', 'figure'),
    Input('df_tasks_storage', 'data'),
    Input('btn-update-card3', 'n_clicks'))
def update_card4(df_asjson, n_clicks):
    if df_asjson is None:
        df = getData('./sqlqueries/submissions_all', isFilepath=True)
    else:
        df = pd.read_json(df_asjson)
        df['taskid'] = df['taskid'].astype(str)
    df100 = df.nlargest(25, 'timestamp')
    counts = df100.groupby(['taskid'])['taskid'].count().reset_index(name='counts')

    import plotly.graph_objects as go

    #[taskTitle, tags, taskDescription] = task.getTaskInformation(taskid)
    labels = ["[{}] {}".format(taskid, task.getTaskInformation(taskid)[0]) for taskid in counts['taskid']]
    values = counts['counts']

    fig = go.Figure(data=[go.Pie(labels=labels, values=values, hole=.5)])
    fig.update_layout(showlegend=True,
        #autosize=False,
        height=300,
        margin=dict(l=0,r=0,b=0,t=0,pad=4),
        #hovermode="x unified",
        hoverlabel=dict(
            bgcolor='#efecec',
            font_size=16,
        ),
    )

    return fig

