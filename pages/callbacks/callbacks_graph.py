from index import app
from pages.users import users
from dash import Input, Output, State
import dash_dangerously_set_inner_html

@app.callback(
    Output("graph-information", "is_open"),
    Input('btn-graph-information', 'n_clicks'),
    State("graph-information", "is_open"),
    prevent_initial_call=False)
def openGraphInformation(n_clicks, is_open):
    if n_clicks:
        return not is_open
    return is_open
