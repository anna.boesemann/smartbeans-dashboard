from index import app
from pages.overview import overview
from pages.tasks import tasks
from pages.users import users
from pages.graph import graph
from pages.data import getData, getTags
from python.users import preprocessData
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate

# ======================== Callbacks 
@app.callback(
    Output('tabs-nav-content', 'children'),
    Input('tabs-nav', 'value'))
def render_content(tab):
    if tab == 'overview':
        return overview()
    elif tab == 'tasks':
        return tasks()
    elif tab == 'users':
        return users()
    elif tab == 'graph':
        return graph()
        
# update storage (tasks data basis)
@app.callback(
    Output('df_tasks_storage', 'data'),
    Output('fig_tasks', 'selectedData'),
    Input('df_tasks_storage', 'data'),
    Input('dropdown_plotselector','value'))
def update_data(data, value):
    print("update_data")
    if data is None:
        df = getData('./sqlqueries/submissions_all', isFilepath=True)
        return [df.to_json(),None]
    elif value in ["Number of Submissions", "Task completion time"]:
        raise PreventUpdate
    else:
        raise PreventUpdate

@app.callback(
    Output('tag_index_storage', 'data'),
    Input('graph-filter-dropdown', 'options'),
    State('tag_index_storage', 'data'))
def update_tagindex(filteroptions, index):
    print("update_tagindex")
    #if index is None:
    #    pass
    index_new = getTags()
    return index_new

@app.callback(
    Output('df_users_storage', 'data'),
    Input('col-user-content', 'children'),
    State('df_users_storage', 'data'))
def update_userdata(children, data):
    print("update_userdata")
    if data is None:
        df_user = getData("./sqlqueries/submissions_peruser", isFilepath=True)
        return df_user.to_json()
    return data

