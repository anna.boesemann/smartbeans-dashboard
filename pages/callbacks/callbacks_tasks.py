from index import app
from pages.tasks import tasks, getLayoutRight, getTagBadge, conditionals
from python import tasks as task
from pages.data import getData
from dash import Input, Output, State, dcc, html, ctx
from dash.exceptions import PreventUpdate

import json 
import pandas as pd
import plotly.graph_objects as go
import plotly.express as px
#import plotly.io as pio
#pio.templates.default = "plotly_white"

# update right layout component
@app.callback(
    Output('col-tasks-right','children'),
    Input('btn-show-table','n_clicks'),
    Input('dropdown-taskid', 'value'))
def updateLayoutRight(n_clicks, inputData):
    print("updateLayoutRight ", inputData)
    input_id = ctx.triggered_id
    taskSelected = False
    if (inputData == None) or (input_id == 'btn-show-table'):
        taskSelected = False
    elif inputData:
        taskSelected = True
    return getLayoutRight(taskSelected)

# update input field: taskid with clickData
@app.callback(
    Output('dropdown-taskid', 'value'),
    Input('btn-show-table','n_clicks'),
    Input('fig_tasks', 'clickData'),
    prevent_initial_call=True)
def update_inputTaskid(n_clicks, clickData):
    print("update_inputTaskid")
    input_id = ctx.triggered_id
    if (input_id == 'fig_tasks') and clickData:
        return clickData['points'][0]['y']
    else: 
        return None

# update graph component
@app.callback(
    Output('fig_tasks', 'figure'),
    Output('task-plot-title', 'children'),
    Output("modal-title", "children"),
    Output("model-content", "children"),
    Input('df_tasks_storage', 'data'),
    Input('tag_index_storage','data'),
    Input('graph-filter-dropdown', 'value'),
    Input('date-slider-tasks', 'value'),
    Input('dropdown_plotselector','value'))
def update_graph(df_asjson, tagindex, filterlist, slider_range, value):
    print("update_graph")
    df_tasks = pd.read_json(df_asjson)
    df_tasks['taskid'] = df_tasks['taskid'].astype(str)
    # date-range filter
    daterange = pd.date_range(start ='10-11-2021', end='10-17-2021', freq='3H')
    low, high = daterange[slider_range[0]], daterange[slider_range[1]]
    time_mask = (df_tasks['date'] > low) & (df_tasks['date'] < high)
    df_tasks = df_tasks[time_mask]
    # tags filter
    if (filterlist is not None) and (len(filterlist) > 0):
        taskid_nestedlist = [tagindex[tag] for tag in filterlist]
        taskidlist = [subitem for item in taskid_nestedlist for subitem in item]
        df_tasks = df_tasks[df_tasks['taskid'].isin(taskidlist)]
    if value == "Number of Submissions":
        fig_tasks = task.explore(df_tasks)
    elif value == "Task completion time":
        fig_tasks = task.getTimePlot(df_tasks)
    else: 
        fig_tasks = go.Figure()
    return [fig_tasks, value, value, dcc.Graph(figure=fig_tasks)]

# update datatable
@app.callback(
    Output('datatable_tasks', 'data'),
    Output('datatable_tasks', 'columns'),
    Output('datatable_tasks', 'style_data_conditional'),
    Input('df_tasks_storage', 'data'),
    Input('tag_index_storage','data'),
    Input('graph-filter-dropdown', 'value'),
    Input('dropdown_plotselector','value'),
    Input('date-slider-tasks', 'value'),
    Input('fig_tasks', 'selectedData'))
def update_datatable(df_asjson, tagindex, filterlist, 
        dropdownSelector, slider_range, selectedData):
    print("update_datatable")
    df_tasks = pd.read_json(df_asjson)
    df_tasks['taskid'] = df_tasks['taskid'].astype(str)
    del df_tasks['timestamp']
    #df_tasks['date'] = pd.DatetimeIndex(df_tasks['date']).strftime("%Y-%m-%d : %H h")

    # date-range filter
    daterange = pd.date_range(start ='10-11-2021', end='10-17-2021', freq='3H')
    low, high = daterange[slider_range[0]], daterange[slider_range[1]]
    time_mask = (df_tasks['date'] > low) & (df_tasks['date'] < high)
    df_tasks = df_tasks[time_mask]
    # tags filter
    if (filterlist is not None) and (len(filterlist) > 0):
        taskid_nestedlist = [tagindex[tag] for tag in filterlist]
        taskidlist = [subitem for item in taskid_nestedlist for subitem in item]
        df_tasks = df_tasks[df_tasks['taskid'].isin(taskidlist)]

    if dropdownSelector == "Number of Submissions":
        df_tasks = task.preprocessData(df_tasks)
        df_tasks['taskid'] = list(df_tasks.index)
        try: 
            df_tasks = df_tasks[['taskid','SUCCESS','WRONG_ANSWER','RUN_ERROR','Total']]
        except:
            pass
        columns = [{"name": i, "id": i} for i in df_tasks.columns]
        keys = []
        if selectedData:
            for p in selectedData['points']:
                keys.append(p['y'])
            return [df_tasks[df_tasks['taskid'].isin(keys)].to_dict('results'),columns,conditionals(df_tasks)]
        return [df_tasks.to_dict('results'), columns,conditionals(df_tasks)]
    elif dropdownSelector == "Task completion time":
        df_time = df_tasks
        del df_time['user']
        columns = [{"name": i, "id": i} for i in df_time.columns]
        return [df_time.to_dict('results'),columns,conditionals(df_time, isTime=True)]
    else: 
        raise PreventUpdate

# update singletask: non-graphics
@app.callback(
    Output('task-title','children'),
    Output('taskid-p-element', 'children'),
    Output('taskcompetencies-div-element', 'children'),
    Output('taskDescription', 'children'),
    Input('dropdown-taskid','value'),
    Input('fig_tasks', 'clickData'))
def update_singletask(inputData, clickData):
    print("update_singletask")
    print("display_clicked_task - callback triggered")
    print("clickData: {}, input_data: {}".format(clickData,inputData))
    if inputData:
        taskid = inputData
        # get Information on task
        [taskTitle, tags, taskDescription] = task.getTaskInformation(taskid)
    elif clickData:
        taskid = clickData['points'][0]['y']
        # get Information on task
        [taskTitle, tags, taskDescription] = task.getTaskInformation(taskid)
        inputData = taskid
    else:
        taskid = "-"
        taskTitle = "No task selected."
        tags = []
        taskDescription = "No task selected."
    taskTitle = "[{}] {}".format(taskid, taskTitle)
    taskid_str = "TaskID: {}".format(taskid)
    taskCompetencies = html.Span([getTagBadge(tag) for tag in tags])
    return [taskTitle, taskid_str, taskCompetencies, taskDescription]

# update singletask: graphics
@app.callback(
    Output('fig_task', 'figure'),
    Input('task-dropdown-plotselector', 'value'),
    Input('dropdown-taskid', 'value'))
def update_singletask_graph(dropdown_value, inputData):
    print("update_singletask_graph")
    if inputData:
        taskid = inputData
        # get Plot
        plotType = 0
        if dropdown_value == "Tries until success": 
            plotType = "triesTillSuccess"
        elif dropdown_value == "Time until success": 
            plotType = "timeTillSuccess"
        fig_task = task.getTaskPlot(taskid, plotType = plotType)
        fig_task.update_layout(title={"text": dropdown_value, 'xanchor': 'center', "yanchor": "bottom", 'y':0.9, 'x':0.5})
    else:
        fig_task = go.Figure()
    return fig_task

# update single task submission table
@app.callback(
    Output('task_submissions', 'data'),
    Output('task_submissions', 'columns'),
    Output('task_submissions', 'tooltip_data'),
    Input('task-dropdown-plotselector', 'value'),
    Input('dropdown-taskid', 'value'))
def update_singletask_submissiontable(dropdown_value, inputData):
    print("update_singletask_submissionstable")
    print(inputData)
    df = pd.DataFrame()
    solution = ""
    if inputData:
        taskid = inputData
        [df,solution] = task.getTaskSubmissions(taskid)
    columns = [{"name": i, "id": i} for i in df.columns]
    tooltip_data = [{column: {'value': str(value), 'type': 'string'}
                        for column, value in row.items()
                    } for row in df.to_dict('records')]
    return [df.to_dict('results'), columns, tooltip_data]

# toggle collapse element - task description
@app.callback(
    Output("collapse-taskdescription", "is_open"),
    Output("btn-collapse-taskdescription", "children"),
    Input("btn-collapse-taskdescription", "n_clicks"),
    State("collapse-taskdescription", "is_open"))
def toggle_collapse(clicked, is_open):
    print("toggle_collapse")
    if clicked:
        is_open = not is_open
        if is_open: 
            return [is_open, "Hide task"]
        else: 
            return [is_open, "Show task"]
    else:
        raise PreventUpdate
    
@app.callback( 
    Output("modal-xl", "is_open"), 
    Input('btn-fullscreen', 'n_clicks'), 
    State("modal-xl", "is_open"), 
    prevent_initial_call=True) 
def openFullScreen(n_clicks, is_open): 
    print("openFullScreen")
    print(n_clicks)
    if n_clicks: 
        return not is_open
    return is_open

@app.callback( 
    Output("tasks-left-information", "is_open"), 
    Input('btn-task-left-information', 'n_clicks'), 
    State("tasks-left-information", "is_open"))
def openTaskLeftInformation(n_clicks, is_open): 
    if n_clicks: 
        return not is_open
    return is_open

@app.callback( 
    Output("tasks-right-information", "is_open"), 
    Input('btn-task-right-information', 'n_clicks'), 
    State("tasks-right-information", "is_open"))
def openTaskRightInformation(n_clicks, is_open): 
    if n_clicks: 
        return not is_open
    return is_open



